const http = require("http");
const path = require("path");
const uuid = require("uuid");
const fs = require("fs");

const port = process.env.PORT || 2000;

const server = http.createServer((request, response) => {

    if (request.url === "/html" && request.method === 'GET') {
        fs.readFile(path.join(__dirname, "content.html"), 'utf-8', (err, htmlData) => {
            if (err) {
                console.error(err);

                response.writeHead(404, { "Content-Type": "text/html" });
                response.write("error while reading html file")
                return response.end();
            } else {
                response.setHeader("Content-type", "text/html");
                response.writeHead(200, { status: "Success" });
                response.write(htmlData);
                return response.end();
            }
        })
    } else if (request.url === "/json" && request.method === 'GET') {
        fs.readFile(path.join(__dirname, "content.json"), (err, jsonData) => {
            if (err) {
                console.log(err);
                response.writeHead(404, { "Content-type": "text/json" })
                response.write("error while reading json file")
                return response.end();
            } else {
                response.setHeader("Content-type", "text/json");
                response.writeHead(200, { status: "Success" });
                response.write(jsonData);
                return response.end();
            }
        })
    } else if (request.url === "/uuid" && request.method === 'GET') {
        const randomUUid = uuid.v4();
        response.setHeader("Content-type", "text/json")
        response.writeHeader(200, { status: "Success" });
        response.write(JSON.stringify({ uuid: randomUUid }));
        return response.end();

    } else if (request.url.startsWith("/status/") && request.method === 'GET') {

        let code = request.url.split("/");
        let statusCode = Number(code[code.length - 1]);
        
        if (typeof (statusCode) !== 'number' || !statusCode) {
            response.writeHead(404, { "Content-type": "text/json" });
            response.write(`not a valid status code`);
            return response.end();
        } else {
            response.writeHead(200, { "Content-type": "text/json" });
            response.write(`${statusCode} : ${http.STATUS_CODES[statusCode]}`);
            return response.end();
        }
    } else if (request.url.startsWith("/delay/") && request.method === 'GET') {

        let requesArr = request.url.split("/");
        number = Number(requesArr[requesArr.length - 1]);

        if (typeof number !== 'number' || !number) {
            response.writeHead(404, { "Content-type": "text/json" });
            response.write('not a valid number provided')
            return response.end()
        } else {
            setTimeout(() => {
                response.writeHead(200, { "Content-type": "text/json" });
                response.write("success")
                return response.end();
            }, number * 1000);
        }
    }
});
server.listen(port, () => {
    console.log("server listening on port" + port);
});

